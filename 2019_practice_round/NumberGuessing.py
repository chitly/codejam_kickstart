n_test_cases = int(input())
for i in range(n_test_cases):
    start_number, last_number = [int(x) for x in input().split()]
    start_number += 1
    n_guesses = int(input())
    for j in range(n_guesses):
        guess_number = (start_number + last_number) // 2
        print(guess_number)
        result = input().strip()
        if result == 'TOO_BIG':
            last_number = guess_number - 1
        elif result == 'TOO_SMALL':
            start_number = guess_number + 1
        else:
            break
