n_test_cases = int(input())
for i in range(n_test_cases):
    n_walls = int(input())
    walls = [int(x) for x in input().strip()]
    
    if n_walls % 2 == 0:
        n_paints = n_walls // 2
    else:
        n_paints = n_walls // 2 + 1
    
    quick_sum_wall = [0]
    for wall in walls:
        quick_sum_wall.append(quick_sum_wall[-1] + wall)
    
    max_score = 0
    for j in range(n_paints, len(quick_sum_wall)):
        sum_score = quick_sum_wall[j] - quick_sum_wall[j - n_paints]
        if sum_score > max_score:
            max_score = sum_score
    print('Case #{}: {}'.format(i + 1, max_score))