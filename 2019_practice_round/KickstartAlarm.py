'''
a   b   c   d
ab  bc  cd            
abc bcd
abcd

4a1^i   +   3b1^i   +   2c1^i   +   1d1^1
            3b2^i   +   2c2^i   +   1d2^i
                        2c3^i   +   1d3^i
                                    1d4^i

4a(1^i) +   3b(1^i + 2^i)   +   2c(1^i + 2^i + 3^i) +   1d(1^i + 2^i + 3^i + 4^i)

for all i
4a[(1^1 + 1^2 + 1^3 + ... + 1^k)] +
3b[(1^1 + 1^2 + 1^3 + ... + 1^k) + (2^1 + 2^2 + 2^3 + ... + 2^k)] +
...
= 4ak + 3b[2(2^(k))//(2-1)] + ...
'''

ANS_MOD = 1000000007

def find_inverse_mod(x, p):
    a = x % p
    if a < 0:
        a += p
    if a == 1:
        return 1
    return (-(p // x) * find_inverse_mod(p % x, p)) % p

def fast_pow(x, n):
    result = 1
    while n > 0:
        if n & 1:
            result = (result * x) % ANS_MOD
        x = (x * x) % ANS_MOD
        n >>= 1
    return result

n_test_cases = int(input())
for i in range(n_test_cases):
    N, K, x1, y1, C, D, E1, E2, F = [int(x) for x in input().split()]
    A = []
    for j in range(N):
        A.append((x1 + y1) % F)
        next_x1 = (C * x1 + D * y1 + E1) % F
        next_y1 = (D * x1 + C * y1 + E2) % F
        x1, y1 = next_x1, next_y1
    
    ans = 0
    sum_of_geo_series = 0
    for j in range(N):
        if j == 0:
            sum_of_geo_series = K
        else:
            r = j + 1
            geo_series = (r * (fast_pow(r, K) - 1) * find_inverse_mod(r - 1, ANS_MOD)) % ANS_MOD
            sum_of_geo_series = (sum_of_geo_series + geo_series) % ANS_MOD
            
        c = N - j
        x = (c * A[j] * sum_of_geo_series) % ANS_MOD
        ans = (ans + x) % ANS_MOD
    print('Case #{}: {}'.format(i + 1, ans))