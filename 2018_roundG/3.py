import heapq
f = open('2018_roundG/sample3.txt')
# f = open('2018_roundG/C-small-practice.in')
wf = open('2018_roundG/sol3.txt', 'w')
t = int(f.readline())
for idx in range(t):
    n, m, e, sr, sc, tr, tc = [int(x) for x in f.readline().split()]
    arr = []
    for i in range(n):
        arr.append([int(x) for x in f.readline().split()])
    q = []
    heapq.heappush(q, (-e, sr - 1, sc - 1, [[False for j in range(m)] for i in range(n)]))
    best = [[100000 for j in range(m)] for i in range(n)]
    best_sol = -1
    # print('case', idx+1, n , m)
    while len(q) > 0:
        k, i, j, collected = heapq.heappop(q)
        next_collected = [[ True if p == i and q == j else collected[p][q] for q in range(m)] for p in range(n)]
        # print(i,j,-k)
        if i == tr - 1 and j == tc - 1:
            if best_sol < -k:
                best_sol = -k
        if best[i][j] <= k:
            continue
        best[i][j] = k
        #left
        if j > 0 and arr[i][j - 1] != -100000:
            if collected[i][j - 1]:
                heapq.heappush(q, (k, i, j - 1, next_collected))
            else:
                heapq.heappush(q, (k - arr[i][j - 1], i, j - 1, next_collected))
        #right
        if j < m - 1 and arr[i][j + 1] != -100000:
            if collected[i][j + 1]:
                heapq.heappush(q, (k, i, j + 1, next_collected))
            else:
                heapq.heappush(q, (k - arr[i][j + 1], i, j + 1, next_collected))
        #up
        if i > 0 and arr[i - 1][j] != -100000:
            if collected[i - 1][j ]:
                heapq.heappush(q, (k, i - 1, j, next_collected))
            else:
                heapq.heappush(q, (k - arr[i - 1][j], i - 1, j, next_collected))
        # down
        if i < n - 1 and arr[i + 1][j] != -100000:
            if collected[i + 1][j]:
                heapq.heappush(q, (k, i + 1, j, next_collected))
            else:
                heapq.heappush(q, (k - arr[i + 1][j], i + 1, j, next_collected))
    wf.write('Case #' + str(idx + 1) + ': ' + str(best_sol) + '\n')
f.close()
wf.close()
