# f = open('2018_roundG/sample2.txt')
f = open('2018_roundG/B-large.in')
wf = open('2018_roundG/sol2_large.txt', 'w')
t = int(f.readline())
for idx in range(t):
    n, q = [int(x) for x in f.readline().split()]
    x1, x2, a1, b1, c1, m1 = [int(x) for x in f.readline().split()]
    y1, y2, a2, b2, c2, m2 = [int(x) for x in f.readline().split()]
    z1, z2, a3, b3, c3, m3 = [int(x) for x in f.readline().split()]
    x = [x1, x2]
    y = [y1, y2]
    z = [z1, z2]
    l, r, k = [], [], []
    for i in range(2, n):
        x.append((a1 * x[i - 1] + b1 * x[i - 2] + c1) % m1)
        y.append((a2 * y[i - 1] + b2 * y[i - 2] + c2) % m2)
    for i in range(2, q):
        z.append((a3 * z[i - 1] + b3 * z[i - 2] + c3) % m3)
    l = [ min(x[i], y[i]) + 1 for i in range(n) ]
    r = [ max(x[i], y[i]) + 1 for i in range(n) ]
    k = [ z[i] + 1 for i in range(q) ]
    sk = sorted(k)
    # print(l)
    # print(r)
    # print(k)
    m = {}
    for x in l:
        if x - 1 in m:
            m[x - 1] -= 1
        else:
            m[x - 1] = -1
    for x in r:
        if x in m:
            m[x] += 1
        else:
            m[x] = 1
    rmk = sorted(m.keys())[::-1]

    cur_idx = 1
    cur_val = rmk[0]
    cur_mul = m[rmk[0]]
    sk_idx = 0
    sol = {}
    for i in range(1, len(rmk)):
        next_val = rmk[i]
        dif_val = cur_val - next_val 
        next_idx = cur_idx + dif_val * cur_mul
        next_mul = cur_mul + m[rmk[i]]
        # print(cur_idx, cur_val, cur_mul)
        while sk_idx < len(sk) and next_idx > sk[sk_idx]:
            s = cur_val - (sk[sk_idx] - cur_idx) // cur_mul
            sol[sk[sk_idx]] = s
            sk_idx += 1
        if sk_idx > len(sk):
            break
        cur_idx = next_idx
        cur_val = next_val
        cur_mul = next_mul
    # print(sol)
    sol_k = [sol[x] if x in sol else 0 for x in k]
    out = 0
    for i in range(q):
        out += sol_k[i] * (i + 1)
    wf.write('Case #' + str(idx + 1) + ': ' + str(out) + '\n')
f.close()
wf.close()
