# f = open('2018_roundG/sample1.txt')
f = open('2018_roundG/A-large.in')
wf = open('2018_roundG/sol1_large.txt', 'w')
t = int(f.readline())
for idx in range(t):
    n = int(f.readline())
    l = [int(x) for x in f.readline().split()]
    sol = 0
    m = {}
    dup = {}
    
    n_one = len([x for x in l if x == 1])
    n_zero = len([x for x in l if x == 0])
    l = [x for x in l if x != 0 and x != 1]
    n = len(l)

    sol += n_one * (n_one - 1) * (n_one - 2) // 6
    sol += n_zero * (n_zero - 1) * (n_zero - 2) // 6
    sol += n_zero * (n_zero - 1) // 2 * (n + n_one)

    for x in l:
        if x in dup:
            dup[x] += 1
        else:
            dup[x] = 1
    for k in dup:
        if dup[k] > 1:
            sol += dup[k] * (dup[k] - 1) // 2 * n_one

    for i in range(n):
        for j in range(i+1, n):
            x = l[i] * l[j]
            if x in m:
                m[x] += 1
            else:
                m[x] = 1
    for i in range(n):
        if l[i] in m:
            sol += m[l[i]]
    wf.write('Case #' + str(idx + 1) + ': ' + str(sol) + '\n')
f.close()
wf.close()