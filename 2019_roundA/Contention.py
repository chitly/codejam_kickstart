# class Fenwick:
#     def __init__(self, length):
#         self._fenwick = [0 for i in range(length + 1)]
#     def add(self, pos, value):
#         fenwick_pos = pos + 1
#         while fenwick_pos < len(self._fenwick):
#             self._fenwick[fenwick_pos] += value
#             fenwick_pos += fenwick_pos & (-fenwick_pos)
#     def get(self, pos):
#         fenwick_pos = pos + 1
#         sum_value = 0
#         while fenwick_pos >= 0:
#             sum_value += self._fenwick[fenwick_pos]
#             fenwick_pos -= fenwick_pos & (-fenwick_pos)

n_test_cases = int(input())
for i in range(n_test_cases):
    n_seats, n_bookings = [int(x) for x in input().split()]
    bookings = []
    for j in range(n_bookings):
        start_pos, end_pos = [int(x) for x in input().split()]
        bookings.append((end_pos - start_pos + 1, start_pos, end_pos))
    bookings.sort()
    # seats = Fenwick(n_seats)
    # for booking in bookings:
    #     n_pos, start_pos, end_pos = booking
    #     seats.add(start_pos - 1, 1)
    #     seats.add(end_pos, -1)

    # Need range update range query eg. segment tree