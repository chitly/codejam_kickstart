# f = open('2018_roundH/sample1.txt')
f = open('2018_roundH/A-large.in')
wf = open('2018_roundH/sol1_large.txt', 'w')
t = int(f.readline())
for idx in range(t):
    n, p = [int(x) for x in f.readline().split()]
    arr = sorted([f.readline().strip() for i in range(p)], key=lambda x: len(x))
    ch = [False for i in range(len(arr))]
    for i in range(len(arr)):
        if ch[i]: continue
        for j in range(i+1, len(arr)):
            if arr[i] == arr[j][:len(arr[i])]:
                ch[j] = True
    sol = 2**n
    for i in range(len(arr)):
        if not ch[i]:
            sol -= 2**(n - len(arr[i]))
    wf.write('Case #' + str(idx + 1) + ': ' + str(sol) + '\n')
f.close()
wf.close()