from math import factorial
# f = open('2018_roundH/sample3.txt')
f = open('2018_roundH/C-large-practice.in')
wf = open('2018_roundH/sol3_large_practice.txt', 'w')

def findInverseMod(x, p):
    a = x % p
    if a < 0: a += p
    if a == 1: return 1
    return (-(p//x) * findInverseMod(p%x, p)) % p

C_MOD = 1000000007
# calc 2**n
pow2 = [1]
for i in range(1, 200001):
    pow2.append((pow2[i-1] * 2) % C_MOD)
    
# calc fac
fac = [1]
for i in range(1, 200001):
    fac.append((fac[i-1] * i) % C_MOD)
    
# calc inv fac
inv_fac = [1]
for i in range(1, 200001):
    # alternative way
    # inv = findInverseMod(i, C_MOD)
    # inv_fac.append((inv_fac[i-1] * inv) % C_MOD)
    inv_fac.append(findInverseMod(fac[i], C_MOD))

t = int(f.readline())
for idx in range(t):
    n, m = [int(x) for x in f.readline().split()]

    p = fac[2*n]
    q = fac[2*n - m - 1]

    r = 0
    for i in range(m):
        x = 2*n - m + i
        q = (q * x) % C_MOD
        y = pow2[m - i] % C_MOD
        # cmr = factorial(m)//factorial(m-i)//factorial(i) % C_MOD
        cmr = (fac[m] * inv_fac[m-i] * inv_fac[i]) % C_MOD

        if i % 2 == 0:
            r += q * y * cmr
        else:
            r -= q * y * cmr
        r %= C_MOD
    
    sol = 0
    if m % 2 == 0:
        sol = (p + r) % C_MOD
    else:
        sol = (p - r) % C_MOD

    if sol < 0:
        sol += C_MOD
        
    wf.write('Case #' + str(idx + 1) + ': ' + str(sol) + '\n')
f.close()
wf.close()