from math import ceil
# f = open('2018_roundH/sample2.txt')
f = open('2018_roundH/B-large.in')
wf = open('2018_roundH/sol2_large.txt', 'w')
t = int(f.readline())
for idx in range(t):
    n = int(f.readline())
    arr = [int(x) for x in f.readline().strip()]
    l = ceil(len(arr) / 2)
    qsl = [sum(arr[:l])]
    for i in range(n - l):
        qsl.append(qsl[i] - arr[i] + arr[l + i])
    sol = max(qsl)
    wf.write('Case #' + str(idx + 1) + ': ' + str(sol) + '\n')
f.close()
wf.close()