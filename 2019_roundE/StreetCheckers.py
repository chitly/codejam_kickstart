primes = [2]
bool_prime = [True for i in range(int(10e6))]
for i in range(3, int(10e6)):
    if not bool_prime[i]:
        continue
    primes.append(i)
    for j in range(3 * i, int(10e6), 2 * i):
        bool_prime[j] = False

memo_primes = set()
def isPrime(x):
    if x % 2 == 0:
        return False
    if x < int(10e6):
        return bool_prime[x]
    if x in memo_primes:
        return True
    for i in primes:
        if i * i > x:
            memo_primes.add(x)
            return True
        if x % i == 0:
            return False

n_test_cases = int(input())
for i in range(n_test_cases):
    l, r = [int(x) for x in input().split()]

    ans = 0
    for j in range(l, r + 1):
        if j % 16 == 0:
            continue
        
        if j % 8 == 0:
            if j == 8:
                ans += 1
        elif j % 4 ==0:
            if j == 4 or isPrime(j // 4):
                ans += 1
        elif j % 2 == 0:
            ans += 1
        else:
            if j == 1 or isPrime(j):
                ans += 1
    
    print('Case #{}: {}'.format(i + 1, ans))
