n_test_cases = int(input())
for i in range(n_test_cases):
    n_cherries, n_black_strands = [int(x) for x in input().split()]
    strands = {x: set() for x in range(1, n_cherries + 1)}

    for j in range(n_black_strands):
        p, q = [int(x) for x in input().split()]
        strands[p].add(q)
        strands[q].add(p)
    
    visited_cherries = set()
    count_used_strand = 0
    count_group = 0
    for j in range(1, n_cherries + 1):
        if j in visited_cherries:
            continue

        count_group += 1
        next_visit = [j]

        while len(next_visit) > 0:
            cur = next_visit[-1]
            visited_cherries.add(cur)

            will_remove_strands = set()
            for k in strands[cur]:
                will_remove_strands.add(k)
                if k not in visited_cherries:
                    count_used_strand += 1
                    next_visit.append(k)
                    break
            else:
                next_visit.pop()
            
            strands[j] = strands[j] - will_remove_strands
    
    min_strand = count_used_strand + 2 * (count_group - 1)
    print('Case #{}: {}'.format(i + 1, min_strand))
