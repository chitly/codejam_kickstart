from bisect import bisect_left

n_test_cases = int(input())
for i in range(n_test_cases):
    n_days, n_slots = [int(x) for x in input().split()]
    
    init_code = 0
    init_eat = 0
    slots = []
    
    for j in range(n_slots):
        code, eat = [int(x) for x in input().split()]
        if code == 0:
            init_eat += eat
        elif eat == 0:
            init_code += code
        else:
            slots.append((code, eat, eat / code))
    slots.sort(key=lambda x: (-x[2], x[0], x[1]))

    curm_sum_eat = [0]
    for j in range(len(slots)):
        code, eat, ratio = slots[j]
        curm_sum_eat.append(curm_sum_eat[-1] + eat)

    curm_sum_code = [0]
    for j in range(len(slots) - 1, -1, -1):
        code, eat, ratio = slots[j]
        curm_sum_code.append(curm_sum_code[-1] + code)
    curm_sum_code = curm_sum_code[::-1]

    ans = []
    for j in range(n_days):
        code, eat = [int(x) for x in input().split()]
        
        if init_code >= code and init_eat >= eat:
            ans.append('Y')
            continue
        
        code -= init_code
        eat -= init_eat

        if code == 0:
            if eat <= curm_sum_eat[-1]:
                ans.append('Y')
            else:
                ans.append('N')
            continue
        
        if eat == 0:
            if code <= curm_sum_code[0]:
                ans.append('Y')
            else:
                ans.append('N')
            continue

        idx = bisect_left(curm_sum_eat, eat)
        if idx == len(curm_sum_eat):
            ans.append('N')
            continue

        code -= curm_sum_code[idx]
        eat -= curm_sum_eat[idx - 1]

        s_code, s_eat, s_ratio = slots[idx - 1]
        if code < (s_eat - eat) * s_code / s_eat + 10e-8:
            ans.append('Y')
        else:
            ans.append('N')
    
    ans = ''.join(ans)
    print('Case #{}: {}'.format(i + 1, ans))
        
